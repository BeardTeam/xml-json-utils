package net.iubris.datautils;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils {

	public static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}

	public static void writeToFile(String stringToWrite, String fileName) throws IOException {
		FileWriter fw = new FileWriter(fileName);
		fw.write(stringToWrite);
		fw.close();
	}
	
	public static void writeStringToNewFile(String originalFile, CharSequence originalExtension, CharSequence newExtension, String transformedString) throws IOException {
		String outputFile = originalFile.replace(originalExtension, newExtension);
		FileUtils.writeToFile(transformedString, outputFile );			
		System.out.println(outputFile+" written");		
	}

}
