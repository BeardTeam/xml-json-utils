package net.iubris.datautils;

import java.io.IOException;
import java.nio.charset.Charset;

import org.json.CDL;
import org.json.JSONArray;

public class Json2Csv implements Transformer {

	public static void main(String[] args) {
		
		String jsonFile = args[0];
		try {
			String jsonFlattenAsString = FileUtils.readFile(jsonFile, Charset.defaultCharset());
			String csv = new Json2Csv().transform( jsonFlattenAsString );
			
			FileUtils.writeStringToNewFile(jsonFile, "json", "csv", csv);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String transform(String dataAsString)  {
		
//		String jsonFlattenAsString = FileUtils.readFile(jsonFile, Charset.defaultCharset());
		
		JSONArray jsonArray = new JSONArray(dataAsString);
		String jsonArrayAsString = CDL.toString( jsonArray );
		return jsonArrayAsString;
	}

}
