package net.iubris.datautils;

import java.io.IOException;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

public class JSON2XML implements Transformer {
	
	public static void main(String[] args) {
		
		try {
			String jsonFile = args[0];
			String jsonFlattenAsString = FileUtils.readFile(jsonFile, Charset.defaultCharset());
			String xml = new JSON2XML().transform( jsonFlattenAsString );
			
			FileUtils.writeStringToNewFile(jsonFile, "json", "xml", xml);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String transform(String fileNameToTransform) {
//		JSONObject jsonObject = new JSONObject(fileNameToTransform);
		JSONArray jsonArray = new JSONArray(fileNameToTransform);
		String xml = "<items>";
		int length = jsonArray.length();
		for (int i=0;i<length;i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			String xmlNode = XML.toString(jsonObject);
			xml += "<item>"+xmlNode+"</item>";
		}
		xml += "</items>";
		return xml;
	}
	
	

}
