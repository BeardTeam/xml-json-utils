package net.iubris.datautils;

public interface Transformer {
	
	String transform(String fileNameToTransform);

}
